﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class startgame : MonoBehaviour
{
    public Text level1balls;
    public Text level2balls;
    public Text level3balls;
    public Toggle level1enabled, level2enabled, level3enabled, musicOn, soundOn;

    //public ShooterPreFinal shooterScriptObject;
    public ShooterSpiroFinal shooterScriptObject;
    public GameObject GoalArea;
    public GameObject GoalPrefab;
    public GameObject ShooterCameraObject;
    public GameObject BasketballGround;
    public GameObject ScoreBoard;
    public GameObject RemainingBallsBoard;
    public GameObject OVRCameraRig;
    public GameObject ConfigCube;
    public GameObject EventSystem;
    public GameObject UIHelpers;
    public GameObject ProgressBarCanvas;
    public GameObject AudioManager;

    public void onPlay() {
        shooterScriptObject.isLevelSelected.Insert(0, level1enabled.isOn);
        shooterScriptObject.isLevelSelected.Insert(1, level2enabled.isOn);
        shooterScriptObject.isLevelSelected.Insert(2, level3enabled.isOn);
        shooterScriptObject.isMusicOn = musicOn.isOn;
        shooterScriptObject.isSoundOn = soundOn.isOn;
        int balls1 = int.Parse(level1balls.text);
        int balls2 = int.Parse(level2balls.text);
        int balls3 = int.Parse(level3balls.text);
        shooterScriptObject.levelBalls.Insert(0, balls1);
        shooterScriptObject.levelBalls.Insert(1, balls2);
        shooterScriptObject.levelBalls.Insert(2, balls3);
        GoalArea.SetActive(true);
        GoalPrefab.SetActive(true);
        ShooterCameraObject.SetActive(true);
        BasketballGround.SetActive(true);
        ScoreBoard.SetActive(true);
        RemainingBallsBoard.SetActive(true);
        EventSystem.SetActive(true);
        AudioManager.SetActive(true);
        ProgressBarCanvas.SetActive(true);
        OVRCameraRig.SetActive(false);
        ConfigCube.SetActive(false);
        UIHelpers.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
