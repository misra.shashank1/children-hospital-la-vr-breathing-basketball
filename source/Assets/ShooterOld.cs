﻿/* Shooter.cs
  version 1.0 - August 7, 2015
  version 1.0.1 - february 7, 2017

  Copyright (C) 2015 Wasabi Applications Inc.
   http://www.wasabi-applications.co.jp/
*/

using UnityEngine;
using System.Collections;
using OVRTouchSample;

public class ShooterOld : MonoBehaviour
{

    public Camera cameraForShooter;
    public GameObject ballPrefab;
    public Transform shotPoint;

    public float targetZ = 12.0f;           //screen point z to world point
    public float shotPowerMin = 3.0f;       //minimum shot power
    public float shotPowerMax = 12.0f;      //maximum shot power
    public float offsetY = 100.0f;          //offset Y for trajectory
    public float shotTimeMin = 0.2f;        //minimum time till to release finger
    public float shotTimeMax = 0.55f;       //maximum time till to release finger
    public float torque = 30.0f;            //torque (backspin)

    public float offsetZShotPoint = 1.0f;   //for rolling ball
    public float powerToRoll = 2.0f;        //for rolling ball
    public float timeoutForShot = 5.0f;     //for error handling

    // for demo
    public float shotPower { get; private set; }        //shot power (initial velocity)
    public Vector3 direction { get; private set; }  //shot direction (normalized)


    GameObject objBall, inhaleBall, preInhaleBall;
    Rigidbody ballRigidbody, inhaleRigidbody, preInhaleRigidbody;
    float startTime;
    bool inhalationComplete = false;

    Vector2 touchPos;

    enum ShotState
    {
        Charging,                   //before shot (rolling)
        //Ready,                      //ready
        //DirectionAndPower,           //on swiping
        Inhale,
        PreInhale,
        Dummy
    }
    ShotState state = ShotState.Dummy;


    // Use this for initialization
    void Start()
    {
        touchPos.x = -1.0f;
    }



    // Update is called once per frame
    void Update()
    {
        OVRInput.Update();
        if (Input.GetKeyDown(KeyCode.A) || OVRInput.GetDown(OVRInput.Button.PrimaryTouchpad))
        {
            if (state == ShotState.Dummy)
            {
                state = ShotState.PreInhale;
                CheckPreInhale();
            }
            else if (state == ShotState.PreInhale)
            {
                Destroy(preInhaleBall);
                state = ShotState.Inhale;
                CheckInhale();
            }
            else if (state == ShotState.Inhale)
            {
                Destroy(inhaleBall);
                state = ShotState.Charging;
                ChargeBall();
            }
        }
    }

    void CheckPreInhale() {
        Vector3 shotPos = shotPoint.transform.localPosition;
        shotPos.z -= offsetZShotPoint;
        Vector3 target_pos = shotPoint.transform.TransformPoint(shotPos);
        print(target_pos.x);    //0
        print(target_pos.y);    //2.05
        print(target_pos.z);    //-1

        // Start is called before the first frame update
        Vector3 initial_pos = new Vector3(3.4f, 0.207f, 5.159f);
        if (preInhaleBall == null)
        {
            inhalationComplete = false;
            preInhaleBall = (GameObject)Instantiate(ballPrefab);
            preInhaleBall.AddComponent<ShotBall>();
            preInhaleBall.SetActive(true);
            preInhaleRigidbody = preInhaleBall.GetComponent<Rigidbody>();
            preInhaleBall.transform.position = initial_pos;
            preInhaleRigidbody.useGravity = false;
            preInhaleRigidbody.AddForce(new Vector3(-3.4f*0.5f,1.633f*0.5f, -5.195f*0.5f), ForceMode.Impulse);

        }
        float dis = Vector3.Distance(target_pos, preInhaleBall.transform.position);
        if (dis <= 2.0f)
        {
            state = ShotState.Inhale;
            Destroy(preInhaleBall);    
        }
    }

    void CheckInhale() {
        Vector3 shotPos = shotPoint.transform.localPosition;
        Vector3 initialPos = shotPoint.transform.TransformPoint(shotPos) + new Vector3(0f, -0.6f, 0f);
        if (inhaleBall == null)
        {
        Debug.Log("Dribble");
            inhaleBall = (GameObject)Instantiate(ballPrefab);
            inhaleBall.AddComponent<ShotBall>();
            inhaleBall.SetActive(true);
            inhaleRigidbody = inhaleBall.GetComponent<Rigidbody>();
            inhaleBall.transform.position = initialPos;
            inhaleBall.transform.position = initialPos;
        }
    }

    void ChargeBall()
    {
        print("Charge Ball Outside");
        if (objBall == null)
        {
            print("Charge Ball Inside");
            objBall = (GameObject)Instantiate(ballPrefab);
            objBall.AddComponent<ShotBall>();
            ballRigidbody = objBall.GetComponent<Rigidbody>();
            objBall.transform.position = shotPoint.transform.position;
            objBall.transform.eulerAngles = shotPoint.transform.eulerAngles;
            ballRigidbody.velocity = Vector3.zero;
            CheckShot();
        }
    }

    void CheckShot()
    {
            if (objBall != null)
            {
                ShootBall(2.0f);
            }

            state = ShotState.Dummy;
            objBall = null;
    }


    void ShootBall(float elapseTime)
    {
        Vector3 worldPoint;

        worldPoint.x = 0.00f;
        worldPoint.y = 0.60f;
        worldPoint.z = 0.80f;

        direction = worldPoint.normalized;
        shotPower = 12.0f;

        ballRigidbody.velocity = direction * shotPower;
        ballRigidbody.AddTorque(-shotPoint.transform.right * torque);

    }

}
