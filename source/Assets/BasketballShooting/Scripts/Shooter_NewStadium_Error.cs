﻿using UnityEngine;
using System.Collections;
using OVRTouchSample;

public class Shooter_NewStadium_Error : MonoBehaviour
{
    public OSC osc;
    public Camera cameraForShooter;
    public GameObject ballPrefab;
    public Transform shotPoint;

    public float targetZ = 12.0f;           //screen point z to world point
    public float shotPowerMin = 3.0f;       //minimum shot power
    public float shotPowerMax = 12.0f;      //maximum shot power
    public float offsetY = 100.0f;          //offset Y for trajectory
    public float shotTimeMin = 0.2f;        //minimum time till to release finger
    public float shotTimeMax = 0.55f;       //maximum time till to release finger
    public float torque = 30.0f;            //torque (backspin)

    public float offsetZShotPoint = 1.0f;   //for rolling ball
    public float powerToRoll = 2.0f;        //for rolling ball
    public float timeoutForShot = 5.0f;     //for error handling

    // for demo
    public float shotPower { get; private set; }        //shot power (initial velocity)
    public Vector3 direction { get; private set; }  //shot direction (normalized)


    GameObject exhaleBall, holdBall, InhaleBall, dummyBall;
    Rigidbody exhaleRigidbody, holdRigidbody,inhaleRigidbody;
    float startTime, inhaleStartTime, holdStartTime, exhaleStartTime;
    bool inhalationComplete = false;
    float breath_value;
    bool isStartDummy = false, isStartInahle = false, isStartHold = false, isStartExhale = false;
    Vector3 dummy_pos = new Vector3(3.4f, 0.207f, 5.159f);
    Vector3 final_inhale_pos, initial_hold_pos, initial_exhale_pos, final_exhale_pos;
    Queue exhBalls;

    enum ShotState
    {
        Exhale,           //on swiping
        Hold,
        Inhale,
        Dummy
    }

    ShotState state = ShotState.Dummy;


    // Use this for initialization
    void Start()
    {
        exhBalls = new Queue();
        state = ShotState.Dummy;
        CheckDummy();
        osc.SetAddressHandler("/m5Analog", OnReceiveSpirometerData);
    }

    void OnReceiveSpirometerData(OscMessage message)
    {
        breath_value = message.GetFloat(0);
        print(breath_value);
    }

    // Update is called once per frame
    void Update()
    {
        OVRInput.Update();
        if (Input.GetKeyDown(KeyCode.A) || OVRInput.GetDown(OVRInput.Button.PrimaryTouchpad))
        {
            if (state == ShotState.Dummy)
            {
                //if (state == ShotState.Dummy && breath_value >= 2300) {
                print("State - Dummy, breath - Inhale");
                if (dummyBall != null)
                    Destroy(dummyBall);
                state = ShotState.Inhale;
                inhalationComplete = false;
                CheckInhale();
                inhaleStartTime = Time.time;
            }

            else if (state == ShotState.Inhale)
            {
                //else if (state == ShotState.Inhale && breath_value >= 2300) {
                //print("State - Inhale, breath - Inhale");
                //if (Time.time - inhaleStartTime >= 1.0f) {
                print("State - Dummy, breath - Inhale, time more than 1");
                inhalationComplete = true;
                if (InhaleBall != null)
                    Destroy(InhaleBall);
                state = ShotState.Hold;
                CheckHold();
                //}
            }

            /* else if (state == ShotState.Inhale && breath_value < 2300) {
                 print("State - Inhale, breath - Not Inhale");
                 inhaleRigidbody.useGravity = true;
                 inhaleRigidbody.AddForce(new Vector3(0, 0, 0), ForceMode.Impulse);
                state = ShotState.Dummy;
                CheckDummy();

             }
         else if (state == ShotState.Hold && breath_value < 2300 && breath_value > 1900) {
             print("State - Hold, breath - Hold");
             if (!isStartHold) {
                 isStartHold = true;
                 holdStartTime = Time.time;
             }
         }*/

            else if (state == ShotState.Hold)
            {
                //else if (state == ShotState.Hold && breath_value <= 1900) {
                print("State - Hold, breath - Exhale");
                //show meter asset
                if (holdBall != null)
                {
                    print("Hold Ball not null");
                    Destroy(holdBall);
                    print("Destroyed hold ball");
                }
                state = ShotState.Exhale;
                ChargeBall();
                print("Ball Charged");
                exhaleStartTime = Time.time;
                print("Start Time " + exhaleStartTime);
            }

            else if (state == ShotState.Exhale)
            {
                //else if (state == ShotState.Exhale && breath_value <= 1900) {
                // increase level on meter asset
                print("State - Exhale, breath - Exhale ");
                print(Time.time - exhaleStartTime + " value - " + breath_value + " state: " + state);
                if ((Time.time - exhaleStartTime) >= 1.0f)
                {
                    // destroy meter
                    print("State - Exhale, breath - Exhale, time more than 1");
                    ShootBall(12.0f);
                    state = ShotState.Dummy;
                    CheckDummy();
                }
            }

            /* else if (state == ShotState.Exhale && breath_value > 1900) {
                 print("State - Exhale, breath - Not Exhale");
                if ((Time.time - exhaleStartTime) < 1.0f)
                {
                    // destroy meter
                    print("State - Exhale, breath - Not Exhale, time less than 1");
                    ShootBall(9.0f);
                    state = ShotState.Dummy;
                    CheckDummy();
                }
                else {
                    print("State - Exhale, breath - Not Exhale, time more than 1");
                    ShootBall(12.0f);
                    state = ShotState.Dummy;
                    CheckDummy();
                }
             }*/
        }
    }

    void CheckDummy()
    {
        Vector3 shotPos = shotPoint.transform.localPosition;
        shotPos.z -= offsetZShotPoint;
        Vector3 target_pos = shotPoint.transform.TransformPoint(shotPos);

        // Start is called before the first frame update
        Vector3 initial_pos = new Vector3(3.4f, 0.207f, 5.159f);
        //Vector3 initial_pos = new Vector3(-53.87f, -1.8f, -41.02f);
        if (dummyBall == null)
        {
            dummyBall = (GameObject)Instantiate(ballPrefab);
            dummyBall.AddComponent<ShotBall>();
            dummyBall.transform.position = initial_pos;
        }

        if (exhBalls.Count > 4) {
            GameObject eb = (GameObject)exhBalls.Dequeue();
            Destroy(eb);
        }
    }

    void CheckInhale() {
        Vector3 shotPos = shotPoint.transform.localPosition;
        shotPos.z -= offsetZShotPoint;
        Vector3 target_pos = shotPoint.transform.TransformPoint(shotPos);
        /*print(target_pos.x);
        print(target_pos.y);
        print(target_pos.z);*/

        // Start is called before the first frame update
        Vector3 initial_pos = new Vector3(3.4f, 0.207f, 5.159f);
        //Vector3 initial_pos = new Vector3(-53.87f, -1.8f, -41.02f);
        //Vector3 final_pos = new Vector3(-46.83f, -0.4f, -36.81f);
        Vector3 final_pos = shotPoint.transform.TransformPoint(shotPos) + new Vector3(0f, -0.6f, 0f);
        Vector3 diff = (final_pos - initial_pos)*0.5f;
        Vector3 worldPoint = cameraForShooter.ScreenToWorldPoint(diff);
        print(worldPoint.x);    //3.576
        print(worldPoint.y);    //4.32
        print(worldPoint.z);    //-6.07


       // if ((inhalationComplete == true && InhaleBall == null) || !inhalationComplete)
        //{
            InhaleBall = (GameObject)Instantiate(ballPrefab);
            InhaleBall.AddComponent<ShotBall>();
            InhaleBall.SetActive(true);
            inhaleRigidbody = InhaleBall.GetComponent<Rigidbody>();
            InhaleBall.transform.position = initial_pos;
            inhaleRigidbody.useGravity = false;
            //inhaleRigidbody.AddForce(new Vector3(-3.4f*0.5f, 1.633f*0.5f, -5.195f*0.5f), ForceMode.Impulse);
            inhaleRigidbody.AddForce(diff, ForceMode.Impulse);
        //}
    }

    void CheckHold() {
         Vector3 shotPos = shotPoint.transform.localPosition;
         Vector3 initialPos = shotPoint.transform.TransformPoint(shotPos) + new Vector3(0f, -0.6f, 0f);
        if (holdBall == null)
        {
            holdBall = (GameObject)Instantiate(ballPrefab);
            holdBall.AddComponent<ShotBall>();
            holdBall.SetActive(true);
            holdRigidbody = holdBall.GetComponent<Rigidbody>();
            holdBall.transform.position = initialPos;
        }
    }

    void ChargeBall()
    {
        print("Inside Charge ball");
        exhaleBall = (GameObject)Instantiate(ballPrefab);
        exhaleBall.AddComponent<ShotBall>();
        exhaleRigidbody = exhaleBall.GetComponent<Rigidbody>();
        exhaleBall.transform.position = shotPoint.transform.position;
        exhaleBall.transform.eulerAngles = shotPoint.transform.eulerAngles;
        exhaleRigidbody.velocity = Vector3.zero;
        exhBalls.Enqueue(exhaleBall);
    }

    void CheckShot()
    {
        if (exhaleBall != null)
        {
            ShootBall(2.0f);
        }

        state = ShotState.Dummy;
        exhaleBall = null;
    }


    void ShootBall(float shotPower)
    {
        Vector3 shotPos = shotPoint.transform.localPosition;
        Vector3 initialPos = shotPoint.transform.TransformPoint(shotPos);
        Vector3 finalPos = new Vector3(-46.4f, 3.020f, -41.758f);
        Vector3 worldPoint;
        worldPoint.x = 0.00f;
        worldPoint.y = 0.60f;
        worldPoint.z = 0.80f;

        //direction = (finalPos - initialPos).normalized;
        direction = worldPoint.normalized;
        //shotPower = 12.0f;

        exhaleRigidbody.velocity = direction * shotPower;
        exhaleRigidbody.AddTorque(-shotPoint.transform.right * torque);
    }
}
