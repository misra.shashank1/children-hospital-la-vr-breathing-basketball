﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using OVR;

public class GoalArea : MonoBehaviour {

	public ParticleSystem psStar;
    public ShooterSpiroFinal spiroFinalScript;
    public SoundFXRef hoopSound, successSound;

    public int score {get; private set;}
    public Text Score;

	// Use this for initialization
	void Start () {
		score = 0;
        Score.text = score.ToString();
	}



	void OnTriggerEnter (Collider other) {
		ShotBall sb = other.GetComponent<ShotBall>();
        if (sb != null) {
            // Goal!!
            if(spiroFinalScript.isSoundOn)
            {
                hoopSound.PlaySound();
            }
            score++;
            Score.text = score.ToString();
            if(spiroFinalScript.isSoundOn)
                StartCoroutine(WaitForHoop());
		}
	}

    IEnumerator WaitForHoop()
    {
        yield return new WaitForSeconds(1);
        successSound.PlaySound();
    }

}
