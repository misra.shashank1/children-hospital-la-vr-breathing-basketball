﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GoalAreaSpiro : MonoBehaviour {

	public ParticleSystem psStar;

	public int score {get; private set;}
    public Text Score;

	// Use this for initialization
	void Start () {
		score = 0;
        Score.text = score.ToString();
	}



	void OnTriggerEnter (Collider other) {
		ShotBall sb = other.GetComponent<ShotBall>();

        if (sb != null) {
            // Goal!!
            Debug.Log("Stupid Particle");
			score++;
            Score.text = score.ToString(); 

			psStar.Play();
		}
	}

}
