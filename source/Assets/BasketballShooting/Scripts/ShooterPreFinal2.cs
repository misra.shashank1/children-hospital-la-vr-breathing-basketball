﻿using UnityEngine;
using System.Collections;
using OVRTouchSample;
using System.Collections.Generic;
using OVR;
using UnityEngine.UI;

public class ShooterPreFinal2 : MonoBehaviour
{
    public SoundFXRef testSound1;
    public SoundFXRef testSound2;
    public List<bool> isLevel;
    public bool isMusic;
    public Text textRemainingBalls;
    int ballsLeft;

    public OSC osc;
    public Camera cameraForShooter;
    public List<float> cameraZOffsets;
    public GameObject ballPrefab;
    public List<Transform> shotPoints;
    public List<GameObject> topStages;
    public List<GameObject> bottomStages;
    public float torque = 30.0f;            //torque (backspin)
    public List<float> shotPowerOptimal;
    public Vector3 exhaleDirection;
    public Vector3 dummyPosition;

    // for demo
    public float shotPower { get; private set; }        //shot power (initial velocity)
    public Vector3 direction { get; private set; }  //shot direction (normalized)


    GameObject exhaleBall, holdBall, InhaleBall, dummyBall;
    Rigidbody exhaleRigidbody, holdRigidbody,inhaleRigidbody;
    float startTime, inhaleStartTime, holdStartTime, exhaleStartTime;
    bool inhalationComplete = false;
    float breath_value;
    bool isStartDummy = false, isStartInahle = false, isStartHold = false, isStartExhale = false;
    int currentLevel = 1;
    public List<float> maxBreathingTime;
    Queue exhBalls;

    public GameObject ProgressBarObject;
    public Image LoadingBar;
    float currentValue;
    public float speed;

    enum ShotState
    {
        Exhale,           //on swiping
        Hold,
        Inhale,
        Dummy
    }

    ShotState state = ShotState.Dummy;


    // Use this for initialization
    void Start()
    {
        cameraForShooter.transform.localPosition = new Vector3(0, 0.7f, 0);
        ballsLeft = 5;
        textRemainingBalls.text = ballsLeft.ToString();
        exhBalls = new Queue();
        setLevel(2);
        dummyPosition = new Vector3(6.36f, 0.19f, 7.46f);
        exhaleDirection = new Vector3(0f, 0.5f, 0.49f);
        state = ShotState.Dummy;
        CheckDummy();
        osc.SetAddressHandler("/m5Analog", OnReceiveSpirometerData);
    }

    void setLevel(int l) {
        currentLevel = l-1;
        for (int i = 0; i < 3; i++) {
            if (i == l - 1)
            {
                topStages[i].SetActive(true);
                bottomStages[i].SetActive(true);
                Vector3 cameraPos = cameraForShooter.transform.position;
                cameraPos.z += cameraZOffsets[currentLevel];
                cameraForShooter.transform.position = cameraPos;
            }
            else {
                topStages[i].SetActive(false);
                bottomStages[i].SetActive(false);
            }
        }
    }

    void OnReceiveSpirometerData(OscMessage message)
    {
        breath_value = message.GetFloat(0);
        print(breath_value);
    }

    // Update is called once per frame
    void Update()
    {
        OVRInput.Update();
        if ((Input.GetKeyDown(KeyCode.A) || OVRInput.GetDown(OVRInput.Button.PrimaryTouchpad)) && ballsLeft >= 0)
        {
            if (state == ShotState.Dummy)
            {
                //if (state == ShotState.Dummy && breath_value >= 2300) {
                print("State - Dummy, breath - Inhale");
                if (dummyBall != null)
                    Destroy(dummyBall);
                state = ShotState.Inhale;
                inhalationComplete = false;
                CheckInhale();
                inhaleStartTime = Time.time;
            }

            else if (state == ShotState.Inhale)
            {
                //else if (state == ShotState.Inhale && breath_value >= 2300) {
                //print("State - Inhale, breath - Inhale");
                //if (Time.time - inhaleStartTime >= 1.0f) {
                print("State - Dummy, breath - Inhale, time more than 1");
                inhalationComplete = true;
                if (InhaleBall != null)
                    Destroy(InhaleBall);
                state = ShotState.Hold;
                CheckHold();
                //}
            }

            /* else if (state == ShotState.Inhale && breath_value < 2300) {
                 print("State - Inhale, breath - Not Inhale");
                 inhaleRigidbody.useGravity = true;
                 inhaleRigidbody.AddForce(new Vector3(0, 0, 0), ForceMode.Impulse);
                state = ShotState.Dummy;
                CheckDummy();

             }
         else if (state == ShotState.Hold && breath_value < 2300 && breath_value > 1900) {
             print("State - Hold, breath - Hold");
             if (!isStartHold) {
                 isStartHold = true;
                 holdStartTime = Time.time;
             }
         }*/

            else if (state == ShotState.Hold)
            {
                //else if (state == ShotState.Hold && breath_value <= 1900) {
                print("State - Hold, breath - Exhale");
                //show meter asset
                if (holdBall != null)
                {
                    print("Hold Ball not null");
                    Destroy(holdBall);
                    print("Destroyed hold ball");
                }


               /* if (currentValue < 100)
                {
                    //currentValue += speed * Time.deltaTime;
                    currentValue += 20f;
                    LoadingBar.fillAmount = currentValue / 100;
                    ProgressBarObject.SetActive(true);
                }
                else
                {
                    ProgressBarObject.SetActive(false);
                    currentValue = 0;
                    LoadingBar.fillAmount = 0;
                    state = ShotState.Exhale;
                    ChargeBall();
                    print("Ball Charged");
                    exhaleStartTime = Time.time;
                    print("Start Time " + exhaleStartTime);
                }*/



                state = ShotState.Exhale;
                ChargeBall();
                print("Ball Charged");
                exhaleStartTime = Time.time;
                print("Start Time " + exhaleStartTime);
            }

            else if (state == ShotState.Exhale)
            {
                //else if (state == ShotState.Exhale && breath_value <= 1900) {
                // increase level on meter asset
                print("State - Exhale, breath - Exhale ");
                print(Time.time - exhaleStartTime + " value - " + breath_value + " state: " + state);
                if ((Time.time - exhaleStartTime) >= 1.0f)
                {
                    //Temporary for the progress bar
                    if (currentValue < 100)
                    {
                        //currentValue += speed * Time.deltaTime;
                        currentValue += 20f;
                        LoadingBar.fillAmount = currentValue / 100;
                        ProgressBarObject.SetActive(true);
                    }
                    else
                    {
                        ProgressBarObject.SetActive(false);
                        currentValue = 0;
                        LoadingBar.fillAmount = 0;
                        float shotPower = shotPowerOptimal[currentLevel];
                        ShootBall(shotPower);
                        state = ShotState.Dummy;
                        CheckDummy();
                    }


                    /*
                    // destroy meter
                    print("State - Exhale, breath - Exhale, time more than 1");
                    float shotPower = shotPowerOptimal[currentLevel];
                    ShootBall(shotPower);
                    //testSound2.PlaySound();
                    state = ShotState.Dummy;
                    CheckDummy();
                    //testSound2.PlaySound();
                    */
                }
            }

            /* else if (state == ShotState.Exhale && breath_value > 1900) {
                 print("State - Exhale, breath - Not Exhale");
                if ((Time.time - exhaleStartTime) < 1.0f)
                {
                    // destroy meter
                    print("State - Exhale, breath - Not Exhale, time less than 1");
                    ShootBall(9.0f);
                    state = ShotState.Dummy;
                    CheckDummy();
                }
                else {
                    print("State - Exhale, breath - Not Exhale, time more than 1");
                    ShootBall(12.0f);
                    state = ShotState.Dummy;
                    CheckDummy();
                }
             }*/
        }
    }

    void CheckDummy()
    {
        Vector3 initial_pos = dummyPosition;
        if (dummyBall == null)
        {
            if (ballsLeft > 0)
            {
                dummyBall = (GameObject)Instantiate(ballPrefab);
                dummyBall.AddComponent<ShotBall>();
                dummyBall.transform.position = initial_pos;
                textRemainingBalls.text = ballsLeft.ToString();
                ballsLeft--;
            }
            else if (ballsLeft == 0) {
                textRemainingBalls.text = ballsLeft.ToString();
                ballsLeft--;
            }
        }

        if (exhBalls.Count > 4) {
            GameObject eb = (GameObject)exhBalls.Dequeue();
            Destroy(eb);
        }
    }

    void CheckInhale() {
        Vector3 initial_pos = dummyPosition;
        Vector3 final_pos = shotPoints[currentLevel].transform.position + new Vector3(0f, -0.6f, 0f);
        Vector3 diff = (final_pos - initial_pos)/maxBreathingTime[currentLevel];


       // if ((inhalationComplete == true && InhaleBall == null) || !inhalationComplete)
        //{
            InhaleBall = (GameObject)Instantiate(ballPrefab);
            InhaleBall.AddComponent<ShotBall>();
            InhaleBall.SetActive(true);
            inhaleRigidbody = InhaleBall.GetComponent<Rigidbody>();
            InhaleBall.transform.position = initial_pos;
            inhaleRigidbody.useGravity = false;
            inhaleRigidbody.AddForce(diff, ForceMode.Impulse);
        //}
    }

    void CheckHold() {
        Vector3 initialPos = shotPoints[currentLevel].transform.position + new Vector3(0f, -0.6f, 0f);
        if (holdBall == null)
        {
            holdBall = (GameObject)Instantiate(ballPrefab);
            holdBall.AddComponent<ShotBall>();
            holdBall.SetActive(true);
            holdRigidbody = holdBall.GetComponent<Rigidbody>();
            holdBall.transform.position = initialPos;
            //testSound1.PlaySound();
        }
    }

    void ChargeBall()
    {
        print("Inside Charge ball");
        exhaleBall = (GameObject)Instantiate(ballPrefab);
        exhaleBall.AddComponent<ShotBall>();
        exhaleRigidbody = exhaleBall.GetComponent<Rigidbody>();
        exhaleBall.transform.position = shotPoints[currentLevel].transform.position;
        exhaleBall.transform.eulerAngles = shotPoints[currentLevel].transform.eulerAngles;
        exhaleRigidbody.velocity = Vector3.zero;
        exhBalls.Enqueue(exhaleBall);
    }

    void ShootBall(float shotPower)
    {
        Vector3 finalPos = new Vector3(-46.4f, 3.020f, -41.758f);
        Vector3 worldPoint = exhaleDirection;
        direction = worldPoint.normalized;

        exhaleRigidbody.velocity = direction * shotPower;
        exhaleRigidbody.AddTorque(-shotPoints[currentLevel].transform.right * torque);
    }


    /*
    Hard Coded Values : 
        Exhale directions and shot Powers - 
                //Original
                Vector3 worldPoint;
                worldPoint.x = 0.00f;
                worldPoint.y = 0.60f;
                worldPoint.z = 0.80f;


                //L1
                Vector3 worldPoint;
                worldPoint.x = 0.00f;
                //worldPoint.y = 0.87f;
                worldPoint.y = 0.50f;
                worldPoint.z = 0.49f;
                shotPower = 9.655f;


                //L2
                Vector3 worldPoint;
                 worldPoint.x = 0.00f;
                 //worldPoint.y = 0.84f;
                 //worldPoint.z = 0.55f;
                 worldPoint.y = 0.50f;
                 worldPoint.z = 0.49f;
                 shotPower = 10.42f;


                //L3
                Vector3 worldPoint;
                worldPoint.x = 0.00f;
                worldPoint.y = 0.50f;
                worldPoint.z = 0.49f;
                shotPower = 12.2f;


        Positions :
            dummyPosition = new Vector3(6.36f, 0.19f, 7.46f);
            exhaleDirection = new Vector3(0f, 0.5f, 0.49f);
            Vector3 finalPos = new Vector3(-46.4f, 3.020f, -41.758f);

       Shot Points Positions :
            l1 pos - (0,0.1,3)
            l1 scale - (1,1,1)
            l2 pos - (0, 0.1, 1)
            l2 scale - (1, 1, 1)
            l3 pos - (0, 0.1, -3.4)
            l3 scale - (1, 1, 1)


        Top Stages : 
            l1 pos - (0, -0.2, 3.79)
            l1 scale - (2, 0.1, 2)
            l2 pos - (0, -0.2, 1.26)
            l2 scale - (2, 0.1, 2)
            l3 pos - 0, -0.2, -3.2)
            l3 scale - (2, 0.1, 2)

        Bottom Stages :
            l1 pos - (0, -1.85, 3.79)
            l1 scale - (2, 0.1, 2)
            l2 pos - (0, -1.85, 1.26)
            l2 scale - (2, 0.1, 2)
            l3 pos - (0, -1.85, -3.2)
            l3 scale - (2, 0.1, 2)

        Camera
            original - 0, 0.7, -3.3
            l1 - 0, 0.7, -1.1
            l2 - 0, 0.7, -3.1
            l3 - 0, 0.7, -8.2
    */

}

