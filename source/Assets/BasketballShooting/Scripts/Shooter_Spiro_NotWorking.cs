﻿using UnityEngine;
using System.Collections;
using OVRTouchSample;

public class Shooter_Spiro_NotWorking : MonoBehaviour

{
    public OSC osc;

    public Camera cameraForShooter;
    public GameObject ballPrefab;
    public Transform shotPoint;
    
    public float shotPowerMin = 3.0f;       //minimum shot power
    public float shotPowerMax = 12.0f;      //maximum shot power

    public float shotTimeMin = 0.2f;        //minimum time till to release finger
    public float shotTimeMax = 0.55f;       //maximum time till to release finger
    public float torque = 30.0f;            //torque (backspin)

    public float offsetZShotPoint = 1.0f;   //for rolling ball
    public float powerToRoll = 2.0f;        //for rolling ball
    public float timeoutForShot = 5.0f;     //for error handling

    // for demo
    public float shotPower { get; private set; }        //shot power (initial velocity)
    public Vector3 direction { get; private set; }  //shot direction (normalized)


    GameObject exhaleBall, holdBall, InhaleBall, dummyBall;
    Rigidbody exhaleRigidbody, holdRigidbody, inhaleRigidbody;
    float startTime, inhaleStartTime, holdStartTime, exhaleStartTime;
    float breath_value = 0.0f;
    bool isStartDummy = false, isStartInahle = false, isStartHold = false, isStartExhale = false;

    Vector3 dummy_pos = new Vector3(3.4f, 0.207f, 5.159f);
    Vector3 final_inhale_pos, initial_hold_pos, initial_exhale_pos, final_exhale_pos;


    enum ShotState
    {
        Exhale,           //on swiping
        Hold,
        Inhale,
        Dummy
    }

    ShotState state = ShotState.Dummy;


    // Use this for initialization
    void Start()
    {
        //CheckDummy();
        osc.SetAddressHandler("/m5Analog", OnReceiveSpirometerData);

        Vector3 shotPos = shotPoint.transform.localPosition;
        shotPos.z -= offsetZShotPoint;
        final_inhale_pos = shotPoint.transform.TransformPoint(shotPos);
    }

    void OnReceiveSpirometerData(OscMessage message)
    {
        breath_value = message.GetFloat(0);
        print("Received breath value: " + breath_value);
    }

    // Update is called once per frame
    void Update()
    {
        print("Received breath value in update: " + breath_value);
        if (state == ShotState.Dummy) {
            while(breath_value >= 2000 && breath_value < 2200)
            {
                if (!isStartDummy) {
                    isStartDummy = true;

                    if(exhaleBall != null)
                        Destroy(exhaleBall);
                    if (InhaleBall != null)
                        Destroy(InhaleBall);
                    if (holdBall != null)
                        Destroy(holdBall);

                    if (dummyBall == null)
                    {
                        dummyBall = (GameObject)Instantiate(ballPrefab);
                        dummyBall.AddComponent<ShotBall>();
                        dummyBall.transform.position = dummy_pos;
                    }
                }
            }
            if (breath_value >= 2200 && breath_value <= 3300) {
                state = ShotState.Inhale;
                isStartDummy = false;
            }
        }

        if (state == ShotState.Inhale)
        {
            while (breath_value >= 2200 && breath_value < 3300)
            {
                if (!isStartInahle)
                {
                    isStartInahle = true;

                    if (dummyBall != null)
                        Destroy(dummyBall);

                    if (InhaleBall == null)
                    {
                        InhaleBall = (GameObject)Instantiate(ballPrefab);
                        InhaleBall.AddComponent<ShotBall>();
                        InhaleBall.SetActive(true);
                        inhaleRigidbody = InhaleBall.GetComponent<Rigidbody>();
                        InhaleBall.transform.position = dummy_pos;
                        inhaleRigidbody.useGravity = false;
                        inhaleRigidbody.AddForce(new Vector3(-3.4f * 0.5f, 1.633f * 0.5f, -5.195f * 0.5f), ForceMode.Impulse);
                    }
                }
                float dis = Vector3.Distance(final_inhale_pos, InhaleBall.transform.position);
                if (dis <= 1.5f)
                {
                    state = ShotState.Hold;
                    Destroy(InhaleBall);
                    
                }
            }
            /*if (breath_value >= 2200 && breath_value <= 3300)
            {
                state = ShotState.Inhale;
                isStartDummy = false;
            }*/
        }

        /* OVRInput.Update();
         if (Input.GetKeyDown(KeyCode.A) || OVRInput.GetDown(OVRInput.Button.PrimaryTouchpad))
         {
             //if (state == ShotState.Dummy && breath_value >= 2200 && breath_value <= 3300)
             if (state == ShotState.Dummy)
             {
                 Destroy(dummyBall);
                 state = ShotState.Inhale;
                 inhaleStartTime = Time.time;
                 CheckInhale();
             }
             //else if (state == ShotState.Inhale && breath_value >= 2000 && breath_value < 2200)
             else if (state == ShotState.Inhale)
             {
                 Destroy(InhaleBall);
                 state = ShotState.Hold;
                 holdStartTime = Time.time;
                 CheckHold();

             }
             //else if (state == ShotState.Hold && breath_value < 2000 && breath_value > 0)
             else if (state == ShotState.Hold)
             {
                 Destroy(holdBall);
                 state = ShotState.Exhale;
                 exhaleStartTime = Time.time;
                 ChargeBall();
             }
             //else if (state == ShotState.Exhale && breath_value >= 2000 && breath_value < 2200) {
             else if (state == ShotState.Exhale)
             {
                 Destroy(exhaleBall);
                 state = ShotState.Dummy;
                 CheckDummy();
             }
         }*/
    }

    /*void CheckDummy()
    {
        Vector3 shotPos = shotPoint.transform.localPosition;
        shotPos.z -= offsetZShotPoint;
        Vector3 target_pos = shotPoint.transform.TransformPoint(shotPos);

        // Start is called before the first frame update
        Vector3 initial_pos = new Vector3(3.4f, 0.207f, 5.159f);
        if (dummyBall == null)
        {
            dummyBall = (GameObject)Instantiate(ballPrefab);
            dummyBall.AddComponent<ShotBall>();
            dummyBall.transform.position = initial_pos;
        }
    }

    void CheckInhale()
    {
        Vector3 shotPos = shotPoint.transform.localPosition;
        shotPos.z -= offsetZShotPoint;
        Vector3 target_pos = shotPoint.transform.TransformPoint(shotPos);

        // Start is called before the first frame update
        Vector3 initial_pos = new Vector3(3.4f, 0.207f, 5.159f);
        if (InhaleBall == null)
        {
            InhaleBall = (GameObject)Instantiate(ballPrefab);
            InhaleBall.AddComponent<ShotBall>();
            InhaleBall.SetActive(true);
            inhaleRigidbody = InhaleBall.GetComponent<Rigidbody>();
            InhaleBall.transform.position = initial_pos;
            inhaleRigidbody.useGravity = false;
            inhaleRigidbody.AddForce(new Vector3(-3.4f * 0.5f, 1.633f * 0.5f, -5.195f * 0.5f), ForceMode.Impulse);

        }

        float dis = Vector3.Distance(target_pos, InhaleBall.transform.position);
        if (dis <= 1.5f)
        {
            print("In here");
            state = ShotState.Hold;
            Destroy(InhaleBall);
        }
    }

    void CheckHold()
    {
        Vector3 shotPos = shotPoint.transform.localPosition;
        Vector3 initialPos = shotPoint.transform.TransformPoint(shotPos) + new Vector3(0f, -0.6f, 0f);
        if (holdBall == null)
        {
            holdBall = (GameObject)Instantiate(ballPrefab);
            holdBall.AddComponent<ShotBall>();
            holdBall.SetActive(true);
            holdRigidbody = holdBall.GetComponent<Rigidbody>();
            holdBall.transform.position = initialPos;
        }
    }

    void ChargeBall()
    {
        if (exhaleBall == null)
        {
            exhaleBall = (GameObject)Instantiate(ballPrefab);
            exhaleBall.AddComponent<ShotBall>();
            exhaleRigidbody = exhaleBall.GetComponent<Rigidbody>();
            exhaleBall.transform.position = shotPoint.transform.position;
            exhaleBall.transform.eulerAngles = shotPoint.transform.eulerAngles;
            exhaleRigidbody.velocity = Vector3.zero;
            CheckShot();
        }
    }

    void CheckShot()
    {
        if (exhaleBall != null)
        {
            ShootBall(2.0f);
        }

        state = ShotState.Dummy;
        exhaleBall = null;
    }


    void ShootBall(float elapseTime)
    {
        Vector3 worldPoint;
        worldPoint.x = 0.00f;
        worldPoint.y = 0.60f;
        worldPoint.z = 0.80f;

        direction = worldPoint.normalized;
        shotPower = 12.0f;

        exhaleRigidbody.velocity = direction * shotPower;
        exhaleRigidbody.AddTorque(-shotPoint.transform.right * torque);
    }*/
}
